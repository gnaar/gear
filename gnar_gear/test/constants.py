ARGON2_DEFAULTS = {
    'time_cost': 2,
    'memory_cost': 102400,
    'parallelism': 8,
    'hash_len': 16,
    'salt_len': 16,
    'encoding': 'utf-8'
}

ENVIRONMENT = {
    'GNAR_LOGLEVEL': 'INFO',
    'GNAR_OFF_PISTE_SERVICE_PORT': '127.0.0.1:9401',
    'GNAR_JWT_ACCESS_TOKEN_EXPIRES_MINUTES': '30',
    'GNAR_JWT_SECRET_KEY': 'SHHH_SUPER_SECRET',
    'GNAR_PG_DATABASE': 'postgres',
    'GNAR_PG_ENDPOINT': '',
    'GNAR_PG_PASSWORD': '',
    'GNAR_PG_USERNAME': 'appUser',
    'GNAR_RECAPTCHA_SECRET_KEY': '',
    'GNAR_SES_ACCESS_KEY_ID': '',
    'GNAR_SES_REGION_NAME': 'us-west-2',
    'GNAR_SES_SECRET_ACCESS_KEY': '',
    'GNAR_SQS_ACCESS_KEY_ID': '',
    'GNAR_SQS_REGION_NAME': 'us-west-2',
    'GNAR_SQS_SECRET_ACCESS_KEY': '',
    'WERKZEUG_RUN_MAIN': 'true'
}

TEST_COMPLEX_PASSWORD = ''.join([chr(int) for int in range(255)])
TEST_SIMPLE_PASSWORD = 'Password'
